package com.example.mymemoapp

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import kotlin.properties.Delegates

class MemoContentProvider : ContentProvider() {

    companion object {
        val AUTHORITY: String =
            "com.example.mymemoapp.MemoContentProvider";
        @kotlin.jvm.JvmField
        val CONTENT_URI: Uri =
            Uri.parse("content://" + AUTHORITY + "/" + MemoContract.Memos.TABLE_NAME);

        // UriMatcher
        val MEMOS = 1;
        val MEMO_ITEM = 2;
        val uriMatcher: UriMatcher = UriMatcher(UriMatcher.NO_MATCH);
        init {
            uriMatcher.addURI(AUTHORITY, MemoContract.Memos.TABLE_NAME, MEMOS);
            uriMatcher.addURI(AUTHORITY, MemoContract.Memos.TABLE_NAME + "/#", MEMO_ITEM);
        }


    }
    private var memoOpenHelper: MemoOpenHelper by Delegates.notNull();


    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        TODO("Implement this to handle requests to delete one or more rows")
    }

    override fun getType(uri: Uri): String? {
        TODO(
            "Implement this to handle requests for the MIME type of the data" +
                    "at the given URI"
        )
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        TODO("Implement this to handle requests to insert a new row.")
    }

    override fun onCreate(): Boolean {
        memoOpenHelper = MemoOpenHelper(context);
        return true;
    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        when (uriMatcher.match(uri)) {
            MEMOS -> null;
            MEMO_ITEM -> null;
            else -> throw IllegalArgumentException("Invalid URI: " + uri);
        }

        var db: SQLiteDatabase = memoOpenHelper.readableDatabase;
        var c: Cursor = db.query(
            MemoContract.Memos.TABLE_NAME,
            projection,
            selection,
            selectionArgs,
            null,
            null,
            sortOrder
        );
        return c;
    }

    override fun update(
        uri: Uri, values: ContentValues?, selection: String?,
        selectionArgs: Array<String>?
    ): Int {
        TODO("Implement this to handle requests to update one or more rows.")
    }
}
