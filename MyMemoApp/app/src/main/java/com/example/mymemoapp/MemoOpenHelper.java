package com.example.mymemoapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MemoOpenHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "myapp_memo.db";
    private static final int DB_VERSION = 1;
    private static final String CREATE_TABLE
                =   "create table " + MemoContract.Memos.TABLE_NAME +  " ("
                    + MemoContract.Memos._ID + " integer primary key autoincrement,  "
                    + MemoContract.Memos.COL_TITLE + " text, "
                    + MemoContract.Memos.COL_BODY + " text, "
                    + MemoContract.Memos.COL_CREATED + " datetime default current_timestamp, "
                    + MemoContract.Memos.COL_UPDATED + "updated datetime default current_timestamp)";

    private static final String INIT_TABLE
                =   "insert into " + MemoContract.Memos.TABLE_NAME
                    + " (" +  MemoContract.Memos.COL_TITLE
                    + ", " + MemoContract.Memos.COL_BODY + ") values "
                    + "('t1', 'b3'), "
                    + "('t2', 'b3'), "
                    + "('t3', 'b3')";


    private static final String DROP_TABLE
                = "drop table if exists " + MemoContract.Memos.TABLE_NAME;

    public MemoOpenHelper(Context c) {
        super(c, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        db.execSQL(INIT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }
}
