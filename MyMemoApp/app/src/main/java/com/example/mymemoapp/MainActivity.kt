package com.example.mymemoapp

import android.content.Intent
import android.database.Cursor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import android.widget.SimpleCursorAdapter
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader

class MainActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<Cursor> {

    companion object {
        @kotlin.jvm.JvmField
        var EXTRA_MYID = "com.example.mymemoapp.MYID";
    }

    private lateinit var adapter: SimpleCursorAdapter;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var from = arrayOf<String>(
                MemoContract.Memos.COL_TITLE,
                MemoContract.Memos.COL_UPDATED
            )

        var to = IntArray(
            2)

        to.set(0, android.R.id.text1)
        to.set(1, android.R.id.text2)

        adapter = SimpleCursorAdapter(
            this,
            android.R.layout.simple_list_item_2,
            null,
            from,
            to,
            0
        )

        var myListView: ListView = findViewById(R.id.myListView);
        myListView.adapter = adapter;
        myListView.onItemClickListener = ListItemClickListener();

        LoaderManager.getInstance(this).initLoader(0, null, this);
        //initLoader(0, null, this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId;

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item);
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        var projection =  arrayOf(
            MemoContract.Memos._ID,
            MemoContract.Memos.COL_TITLE,
            MemoContract.Memos.COL_UPDATED
        );

        return androidx.loader.content.CursorLoader(
            this,
            MemoContentProvider.CONTENT_URI,
            projection,
            null,
            null,
            MemoContract.Memos.COL_UPDATED + " DESC"
        );
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor?) {
        adapter.swapCursor(data);
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        adapter.swapCursor(null);
    }

    private inner class ListItemClickListener : AdapterView.OnItemClickListener {
        override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            var intent = Intent(applicationContext, FormActivity::class.java);
            intent.putExtra(EXTRA_MYID, id);
            startActivity(intent);
        }
    }
}
