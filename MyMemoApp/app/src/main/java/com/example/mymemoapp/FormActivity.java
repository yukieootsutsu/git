package com.example.mymemoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mymemoapp.MemoContract.Memos;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import kotlin.collections.UArraySortingKt;
import kotlin.jvm.internal.markers.KMutableList;


public class FormActivity extends AppCompatActivity {

    private android.content.Intent Intent;
    private Long memoId;

    private EditText titleText;
    private EditText bodyText;
    private TextView updatedText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        titleText = (EditText) findViewById(R.id.titleText);
        bodyText = (EditText) findViewById(R.id.bodyText);
        updatedText = (TextView) findViewById(R.id.updatedText);


        intent: Intent = getIntent();
        memoId = getIntent().getLongExtra(MainActivity.EXTRA_MYID, 0L);

        if (memoId == 0) {
            // new memo
        } else {
            // show memo
            Uri uri = ContentUris.withAppendedId(
                    MemoContentProvider.CONTENT_URI,
                    memoId
            );

            String[] projection = new String[]{
                    Memos.COL_TITLE,
                    Memos.COL_BODY,
                    Memos.COL_UPDATED
            };

            Cursor c = getContentResolver().query(
                    uri,
                    projection,
                    Memos._ID + " = ?",
                    new String[] {Long.toString(memoId)},
                    null
            );

            c.moveToFirst();

            titleText.setText(
                    c.getString(c.getColumnIndex((Memos.COL_TITLE)))
            );
            bodyText.setText(
                    c.getString(c.getColumnIndex((Memos.COL_BODY)))
            );
            updatedText.setText(
                    c.getString(c.getColumnIndex((Memos.COL_UPDATED)))
            );
            c.close();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
